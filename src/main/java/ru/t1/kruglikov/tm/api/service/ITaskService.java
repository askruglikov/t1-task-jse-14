package ru.t1.kruglikov.tm.api.service;

import ru.t1.kruglikov.tm.api.repository.ITaskRepository;
import ru.t1.kruglikov.tm.enumerated.Sort;
import ru.t1.kruglikov.tm.enumerated.Status;
import ru.t1.kruglikov.tm.model.Task;

import java.util.List;

public interface ITaskService extends ITaskRepository {

    List<Task> findAll(Sort sort);

    Task create(String name, String description);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task changeStatusByIndex(Integer index, Status status);

    Task changeStatusById(String id, Status status);

}
