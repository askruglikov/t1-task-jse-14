package ru.t1.kruglikov.tm.api.repository;

import ru.t1.kruglikov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
